# GIT
git clone https://bitbucket.org/ev45ive/angular-open-czerwiec.git
cd angular-open-czerwiec
npm i 
npm start

# Instalacja
npm i -g @angular/cli

echo $PATH

ng help

ng new angular-open-czerwiec

? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS 
[ [https://sass-lang.com/documentation/syntax#scss](https://sass-lang.com/documentation/syntax#scss) ]

# Local dev server
$ ng s -o
The serve command requires to be run in an Angular project, but a project definition could not be found.

# VSCODE
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2
https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode
https://marketplace.visualstudio.com/items?itemName=stringham.move-ts
https://marketplace.visualstudio.com/items?itemName=infinity1207.angular2-switcher
https://marketplace.visualstudio.com/items?itemName=johnpapa.angular-essentials

# Chrome
https://augury.rangle.io/

# Prettier
.prettierrc
{
  "printWidth": 120,
  "singleQuote": true,
  "useTabs": false,
  "tabWidth": 2,
  "semi": true,
  "bracketSpacing": true
}

# ng generate
ng g
ng g m --help
ng g c --help

ng g m playlists -m app --routing
ng g c playlists/views/playlists
ng g c playlists/components/playlists-list
ng g c playlists/components/playlists-list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form


# CSS
/src/styles.scss
::ng-deep 
:host-c.ontext
:host
npm install bootstrap
https://ng-bootstrap.github.io/#/components/collapse/examples
https://ng-bootstrap.github.io/#/home
https://material.angular.io/
https://www.primefaces.org/primeng/
https://www.telerik.com/kendo-angular-ui

# Core
ng g m core -m app --routing
ng g interface core/models/playlist

# Pipes
ng g m shared -m playlists
ng g p shared/yesno --export

# Music Search
ng g m music-search --m app --routing --route music
ng g c music-search/views/music-search
ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card

# MOdel
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

# Fixtures
https://github.com/marak/Faker.js/
https://github.com/boo1ean/casual

# Services
ng g s music-search/services/music-search --flat false

# Auth
https://developer.spotify.com/documentation/general/guides/authorization-guide/
ng g m core/security -m core 
ng g s core/security/auth

https://me.me/i/dogberts-tech-support-hey-it-hello-shut-up-work-and-13299856

# RxJS
https://rxjs-dev.firebaseapp.com/
https://rxmarbles.com/#interval
https://rxviz.com/
https://gist.github.com/staltz/868e7e9bc2a7b8c1f754
https://www.learnrxjs.io/learn-rxjs/operators/utility/topromise
https://rxjs-dev.firebaseapp.com/operator-decision-tree


# Interceptors
ng g interceptor core/security/auth
