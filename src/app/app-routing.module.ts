import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule, ROUTES } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'playlists',
    // pathMatch:'prefix'
    pathMatch: 'full'
  },
  {
    path: 'music',
    loadChildren: () => import('./music-search/music-search.module')//
      .then(m => m.MusicSearchModule)
  },
  {
    path: '**',
    // component: PageNotFoundComponent
    redirectTo: 'music',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
  // constructor(@Inject(ROUTES) routes: any) {
  //   console.log(routes)
  // }
}
