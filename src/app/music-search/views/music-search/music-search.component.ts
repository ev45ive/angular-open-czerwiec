import { Component, OnInit, Inject } from '@angular/core';
import { Album } from 'src/app/core/models/album';
import { MusicSearchService } from '../../services/music-search.service';
import { tap, takeUntil, multicast, refCount, publishReplay, share, shareReplay, map } from 'rxjs/operators';
import { Subscription, Subject, from, ConnectableObservable, ReplaySubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.scss']
})
export class MusicSearchComponent implements OnInit {

  results = this.service.resultsChanges.pipe(shareReplay());
  query = this.service.queryChanges

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService) {
  }


  ngOnInit(): void {
    this.route.queryParamMap
    .pipe(
      map(qpm => qpm.get('q'))
    )
    .subscribe(query => {
      query && this.search(query)
    })
  }

  search(query: string) {
    // this.service.search(query)

    // this.router.navigate(['music', 'search'], {
    this.router.navigate([], {
      queryParams: { q: query },
      relativeTo: this.route
    })
  }

}
