import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { filter, distinctUntilChanged, debounceTime, mergeAll } from 'rxjs/operators';
import { from, Observable } from 'rxjs';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnChanges {

  @Input()
  query = ''

  ngOnChanges(changes: SimpleChanges): void {
    const q = changes['query'].currentValue;
    (this.queryForm.get('query') as FormControl).setValue(q, {
      // onlySelf:true
      emitEvent: false
    })
  }

  // ReactiveFormsModule,
  queryForm = new FormGroup({
    query: new FormControl('', {
      // updateOn:'submit'
      validators: [
        // Validators.pattern('[a-zA-Z]'),
        Validators.required,
        Validators.minLength(3)
      ]
    }),
  })

  constructor() {
    (window as any).form = this.queryForm

    // this.queryForm.reset({},{})

    const field = this.queryForm.get('query')!;

    from([
      this.searchClick,
      field.valueChanges as Observable<string>
    ]).pipe(
      mergeAll(),
      debounceTime(400),
      filter(query => query?.length >= 3),
      distinctUntilChanged()
    )
      .subscribe(this.search)

  }

  ngOnInit(): void {
  }

  @Output() search = new EventEmitter<string>();
  private searchClick = new EventEmitter<string>();

  sendSearch() {
    // console.log(this.queryForm.value)
    this.searchClick.emit(this.queryForm.get('query')!.value)
  }

}
