import { Injectable, Inject, EventEmitter } from '@angular/core';
import { Album, AlbumSearchResults } from 'src/app/core/models/album';
import { SEARCH_API_URL } from './tokens';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Subscription, from, EMPTY, of, throwError, Subject, ReplaySubject, BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/core/security/auth.service';

import { map, pluck, catchError, tap, merge, mergeAll, concatAll, startWith, switchAll, exhaust, mergeMap, concatMap, switchMap, exhaustMap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
  // providedIn: MusicSearchModule
})
export class MusicSearchService {

  private results = new BehaviorSubject<Album[]>(mock as Album[])
  private query = new BehaviorSubject<string>('batman')

  // Output
  resultsChanges = this.results.asObservable()
  queryChanges = this.query.asObservable()

  constructor(
    @Inject(SEARCH_API_URL) private api_url: string,
    private http: HttpClient,
  ) {
    (window as any).results = this.results

    this.query.pipe(
      map(query => ({ q: query, type: 'album' })),
      switchMap(params => this.fetchSearchResults(params)),
    ).subscribe(this.results)

  }

  fetchSearchResults(params: { q: string; type: string; }) {
    return this.http.get<AlbumSearchResults>(this.api_url, { params }).pipe(

      map((res) => res.albums.items),
      catchError(err => EMPTY))

  }

  // Service Input
  search(query: string) {
    this.query.next(query)
  }
}


export const mock = [
  {
    id: '123',
    name: 'Test Album ABC',
    images: [
      { url: 'https://www.placecage.com/c/300/300' }
    ]
  },
  {
    id: '123',
    name: 'Test Album 234',
    images: [
      { url: 'https://www.placecage.com/c/300/300' }
    ]
  },
  {
    id: '123',
    name: 'Test Album ZZZ',
    images: [
      { url: 'https://www.placecage.com/c/300/300' }
    ]
  }
] as Partial<Album>[]