import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MusicSearchComponent } from './views/music-search/music-search.component';
import { SyncComponent } from './views/sync/sync.component';


/* /music/** */
const routes: Routes = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full'
  },
  {
    path: 'search',
    component: MusicSearchComponent
  },
  {
    path: 'sync',
    component: SyncComponent
  },
  // {
  //   path: '**',
  //   // component: PageNotFoundComponent
  //   redirectTo: 'search',
  //   pathMatch: 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }
